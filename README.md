# electron-vue

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```
# electron package
sudo electron-packager . electron-vue --overwrite --platform linux --arch x64 --prune true --out release-builds --electron-version 1.0.0

# debian package
sudo electron-installer-debian --src release-builds/electron-vue-linux-x64/ --arch amd64 --config linux-config.json

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
