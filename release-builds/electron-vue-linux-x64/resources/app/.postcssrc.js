// https://github.com/michael-ciniawsky/postcss-load-config
const path = require('path')

module.exports = {
  "plugins": {
    "postcss-import": {},
    "postcss-url": {},
    // to edit target browsers: use "browserslist" field in package.json
    "autoprefixer": {}
  },
  output: {
    filename: 'assets/scripts/[name].js',
    libraryTarget: 'commonjs2',
    path: path.join(__dirname, '/dist/')
  },
}
