var url = require('url')
const path = require('path');
const {app, BrowserWindow, Menu} = require('electron')

app.on('ready', function () {

  // Initialize the window to our specified dimensions
  win = new BrowserWindow({
    width: 1000,
    height: 600,
    webPreferences: {
      nodeIntegration: false,
      preload: './src/main.js'
    }
  });

  // win.loadURL(url.format({
  //   pathname: path.join(__dirname, 'dist/index.html'),
  //   protocol: 'file:',
  //   slashes: true
  // }));

  // Specify entry point to default entry point of vue.js
  win.loadURL('http://localhost:8080');

  menuTemplate = Menu.buildFromTemplate(template)
  Menu.setApplicationMenu(menuTemplate)

  // Remove window once app is closed
  win.on('closed', function () {
    win = null;
  });


});
//create the application window if the window variable is null
app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
})
//quit the app once closed
app.on('window-all-closed', function () {
  if (process.platform != 'darwin') {
    app.quit();
  }
});

const template = [
  {
    label: app.getName(),
      submenu: [
        {role: 'about'},
        // {type: 'separator'},
        // {role: 'services', submenu: []},
        {type: 'separator'},
        {role: 'hide'},
        {role: 'hideothers'},
        {role: 'toggledevtools'},
        {type: 'separator'},
        {role: 'quit'}
      ]
  },
  {
    role: 'window',
    submenu: [
      {role: 'minimize'},
      {role: 'windowMenu'},
      {role: 'togglefullscreen'},
      {role: 'close'}
    ]
  },
]