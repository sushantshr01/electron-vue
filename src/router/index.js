import Vue from 'vue'
import Router from 'vue-router'
import Drag from '@/components/Drag/Drag'
import Index from '@/components/'
import Slick from '@/components/Image/vue-slick'
import FluxSlider from '@/components/Image/FluxImageSlider'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/drag',
      name: 'Drag',
      component: Drag
    },
    {
      path: '/vue-slick',
      name: 'Slick',
      component: Slick
    },
    {
      path: '/vue-flux',
      name: 'FluxSlider',
      component: FluxSlider
    }
  ]
})
